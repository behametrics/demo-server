#!/bin/sh

curl -s -X POST -H 'Content-type: application/json' --data '{"username": "'$CI_PROJECT_NAME'", "text": "Version <'$CI_PIPELINE_URL'|'$CI_PIPELINE_ID'> deployed to '$1'"}' $SLACK_WEBHOOK
