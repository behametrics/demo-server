from .auth import blueprint as auth
from .main import blueprint as main

__all__ = ['main', 'auth']
