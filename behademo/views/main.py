from flask import Blueprint

blueprint = Blueprint('main', __name__)


@blueprint.route('/')
def root():
    return 'behademo-server'
