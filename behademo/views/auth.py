from flask import Blueprint
from flask import current_app as app
from flask import jsonify
from webargs import fields
from webargs.flaskparser import use_kwargs

from behademo.error import InvalidSession
from behademo.error import ModelNotFound
from behademo.services.auth.tasks import predict_for_user
from behademo.services.auth.tasks import train_model_for_user
from behademo.services.dao import get_user_models_dao
from behademo.services.server_service import get_logger_server_client

blueprint = Blueprint('authentication', __name__)


@blueprint.route('/train', methods=['POST'])
@use_kwargs({
    'username': fields.Str(required=True)
})
def train(username):
    train_model_for_user(username)
    app.logger.debug(f'Task train_model_for_user(username={username}) '
                     f'pushed to queue')
    return '', 202


@blueprint.route('', methods=['GET'])
@use_kwargs({
    'username': fields.Str(required=True),
    'sessionId': fields.Str(required=True)
})
def predict(username, sessionId):
    ensure_valid_session_id(sessionId)
    ensure_model_exists(username=username)

    is_authenticated = predict_for_user(username, sessionId)

    return jsonify({
        'sessionId': sessionId,
        'username': username,
        'isAuthenticated': int(is_authenticated)
    })


def ensure_model_exists(username):
    exists = get_user_models_dao().exists({
        'username': username
    })
    if not exists:
        raise ModelNotFound(username=username)


def ensure_valid_session_id(session_id):
    client = get_logger_server_client()
    session = client.get_session(session_id)

    if session is None:  # TODO
        raise InvalidSession(session_id=session_id)
