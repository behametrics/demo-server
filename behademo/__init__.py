from flask import Flask
from flask_cors import CORS

from behademo.config import Configuration


def create_app():
    app = Flask(__name__)
    app.config.from_object(Configuration)
    register_plugins(app)
    register_blueprints(app)
    register_error_handlers(app)

    return app


def register_blueprints(app):
    from .views import main, auth
    app.register_blueprint(main, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/auth')


def register_plugins(app):
    CORS(app)


def register_error_handlers(app):
    from . import error
    app.register_blueprint(error.blueprint)
