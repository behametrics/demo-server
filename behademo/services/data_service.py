from io import StringIO

import pandas as pd

from .server_service import get_logger_server_client

_client = get_logger_server_client()


def get_mouse_data_for_session(session_id):
    data = _client.get_data_csv_device(query={'sessionId': session_id},
                                       device='mouse')

    df = pd.read_csv(StringIO(data), sep=';')
    df = df.rename(columns={
        'sessionId': 'sessionId',
        'eventType': 'eventType',
        'payload_positionX': 'x',
        'sessionStartsAt': 'sessionStartsAt',
        'time': 'time',
        'payload_positionY': 'y',
        'payload_mouseButton': 'payload_mouseButton'
    })

    df = df.drop(columns=['payload_scrollDeltaX', 'payload_scrollDeltaY'],
                 errors='ignore')
    df['username'] = _client.get_session(session_id).get('username')

    return df


def get_n_other(username, n):
    usernames = _client.get_usernames()

    usernames.remove(username)

    count = min(n, len(usernames))

    df = pd.DataFrame()
    for i, j in enumerate(usernames):
        if i >= count:
            break

        for s in _client.get_sessions_for_user_auth(j):
            df = df.append(get_mouse_data_for_session(s),
                           ignore_index=True)

    return df
