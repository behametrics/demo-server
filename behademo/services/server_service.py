from typing import Mapping, List

import json
import requests


def get_logger_server_client(
        api_root='https://team04-18.studenti.fiit.stuba.sk/'
                 'logger-server/prod'):
    return LoggerServerClient(api_root=api_root)


class LoggerServerClient:

    def __init__(self, api_root):
        self.api_root = api_root

    @staticmethod
    def dump_param_value(value):
        if isinstance(value, dict):
            return json.dumps(value)
        return value

    def _fetch_data(self, path, query: Mapping = None, body=None):
        if query is None:
            query = {}
        else:
            query = {k: LoggerServerClient.dump_param_value(v)
                     for k, v in query.items()}

        res = requests.get(url=f'{self.api_root}/{path}',
                           params=query,
                           data=body)
        return res

    def _fetch_data_text(self, path, query: Mapping = None, body=None):
        if query is None:
            query = {}
        return self._fetch_data(path, query, body).text

    def _fetch_data_json(self, path, query: Mapping = None, body=None):
        if query is None:
            query = {}
        return self._fetch_data(path, query, body).json()

    def get_data(self, query: Mapping):
        return self._fetch_data_json(
            path='data',
            query=query)

    def get_data_csv(self, query: Mapping, columns: List):
        return self._fetch_data_text(
            path='data/csv',
            query={
                'query': query,
                'columns': ','.join(columns)
            })

    def get_data_csv_device(self, query: Mapping, device):
        return self._fetch_data_text(
            path=f'data/csv/{device}',
            query={
                'query': query
            }
        )

    def get_session(self, session_id):
        return self._fetch_data_json(
            f'session/{session_id}'
        )

    def get_sessions_for_user_auth(self, username):
        return self._fetch_data_json(
            f'session/for-user',
            query={
                'username': username
            }
        )

    def get_usernames(self):
        return self._fetch_data_json(
            f'session/usernames-list'
        )

    def get_columns(self):
        return self._fetch_data_json(
            path='columns')
