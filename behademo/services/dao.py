import pickle
from abc import ABC

from bson.binary import Binary
from pymongo import DESCENDING
from pymongo import MongoClient

from behademo.config import Configuration

_client = MongoClient(Configuration.MONGO_URI)


def get_user_models_dao(client=_client):
    return UserModelsDao(client=client)


class Dao(ABC):
    def __init__(self, collection_name, client=_client):
        self._db = client.get_database()
        self._collection_name = collection_name

    @property
    def _collection(self):
        return self._db.get_collection(self._collection_name)

    @staticmethod
    def _stringify_oid(obj):
        if obj is None:
            return obj

        obj['_id'] = str(obj['_id'])
        return obj

    def save(self, data):
        if isinstance(data, list):
            return self.save_batch(data)
        else:
            return self.save_one(data)

    def save_batch(self, obj_list):
        self._collection.insert_many(obj_list)
        return [self._stringify_oid(obj) for obj in obj_list]

    def save_one(self, json):
        self._collection.insert_one(json)
        return self._stringify_oid(json)

    def get(self, query=None):
        if query is None:
            query = {}
        return [self._stringify_oid(i) for i in self.get_cursor(query)]

    def get_cursor(self, query=None, **kwargs):
        if query is None:
            query = {}
        return self._collection.find(query, **kwargs)

    def get_one(self, query=None):
        if query is None:
            query = {}
        return self._stringify_oid(self._collection.find_one(query))

    def update_one(self, update, criteria=None, **kwargs):
        if criteria is None:
            criteria = {}
        return self._collection.update_one(criteria, update, **kwargs)

    def exists(self, query=None):
        if query is None:
            query = {}
        return self.get_one(query) is not None

    def exists_for_id(self, object_id):
        query = {
            '_id': object_id
        }

        return self.exists(query)


class UserModelsDao(Dao):

    def __init__(self, client=_client):
        super().__init__(client=client, collection_name='user_models')

    def save_model(self, username, model):
        record = {
            'username': username,
            'model_bin': Binary(pickle.dumps(model))
        }
        return self.save(record)

    def get_model(self, username):
        query = {
            'username': username
        }

        # returns latest model or None
        for record in self.get_cursor(query, limit=1,
                                      sort=[('_id', DESCENDING)]):
            return pickle.loads(record['model_bin'])
        return None
