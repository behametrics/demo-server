from behademo.celery_config import celery
from behademo.services.auth.user_model import train_model, predict
from behademo.services.dao import get_user_models_dao


def train_model_for_user(username):
    return train_save_model.delay(username)


def predict_for_user(username, session_id):
    model = get_user_models_dao().get_model(username)
    return predict(username, session_id, model)


@celery.task
def train_save_model(username):
    model = train_model(username)
    get_user_models_dao().save_model(username, model)
