import random


class RandomClassifier:

    def fit(self, *args, **kwargs):
        return self

    def predict(self, *args, **kwargs):
        return random.randint(0, 1)
