from sklearn.tree import DecisionTreeClassifier
from behalearn.authentication import UserLabelIdentifier
from behalearn.preprocessing import CustomSegmentSplitter
from behalearn.preprocessing import SegmentSplitter
from behalearn.preprocessing import SpeedFeaturesCalculator
from behalearn.preprocessing import AngularVelocityCalculator
from behalearn.features import SegmentFeatureCalculator
from behalearn.estimator import VotingSegmentClassifier
from behalearn.preprocessing.segment import criteria_by_time_interval
from behademo.services import data_service


def train_model(username):
    # load data
    data = data_service.get_n_other(username, 5)

    # preprocessing
    X, y = preprocess_for_auth(data, username)

    model = VotingSegmentClassifier(DecisionTreeClassifier)
    model.fit(X, y)
    return model


def predict(username, session_id, model):
    # load data from session_id
    data = data_service.get_mouse_data_for_session(session_id)

    # preprocessing
    X, _ = preprocess_for_auth(data, username)

    return model.predict(X)


def preprocess_for_auth(data, username):
    move_id_col_name = 'seg_move_id'
    auth_pipe = _auth_pipeline(move_id_col_name)

    # identify user (1 if authorized, 0 otherwise)
    auth_pipe['labeler'].fit(data, user_id=username)
    data = auth_pipe['labeler'].transform(data)

    # select only logs between mouse UP and DOWN
    data = auth_pipe['move_splitter'].transform(data)
    data.dropna(subset=[move_id_col_name], inplace=True, axis=0)

    # identify segments
    data = auth_pipe['segment_splitter'].transform(data)

    # compute speed columns
    data = auth_pipe['vel_calculator'].transform(data)
    data = auth_pipe['acc_calculator'].transform(data)
    data = auth_pipe['jerk_calculator'].transform(data)
    data = auth_pipe['angular_vel_calculator'].transform(data)

    data.dropna(inplace=True, axis=0, how='all',
                subset=['velocity_x', 'velocity_y', 'velocity_xy',
                        'acceleration_x', 'acceleration_y', 'acceleration_xy',
                        'jerk_x', 'jerk_y', 'jerk_xy',
                        'angular_velocity_xy'])

    segment_length = data['segment'].value_counts()
    segment_filtered = segment_length[segment_length >= 4].index
    data = data.loc[data['segment'].isin(segment_filtered)]

    # compute features from speed columns
    features = auth_pipe['feature_calculator'].transform(data)

    features = features.merge(data[['segment', 'user_label']], on='segment') \
        .drop(columns='segment')

    X = features.drop(columns='user_label')
    y = features['user_label']
    return X, y


def _auth_pipeline(move_id_col):
    return {
        'labeler': UserLabelIdentifier(),
        'move_splitter': CustomSegmentSplitter(
            res_col_name=move_id_col,
            begin_criteria={'eventType': 'MOUSE_DOWN'},
            end_criteria={'eventType': 'MOUSE_UP'}
        ),
        'segment_splitter': SegmentSplitter(
            'segment', move_id_col,
            [(criteria_by_time_interval, {'time_interval': 150})]
        ),
        'vel_calculator': SpeedFeaturesCalculator(
            'segment', prefix='velocity', columns=['x', 'y']
        ),
        'acc_calculator': SpeedFeaturesCalculator(
            'segment', prefix='acceleration',
            columns=['velocity_x', 'velocity_y']
        ),
        'jerk_calculator': SpeedFeaturesCalculator(
            'segment', prefix='jerk',
            columns=['acceleration_x', 'acceleration_y']
        ),
        'angular_vel_calculator': AngularVelocityCalculator(
            'segment', prefix='angular_velocity', columns=['x', 'y']
        ),
        'feature_calculator': SegmentFeatureCalculator('segment')
    }
