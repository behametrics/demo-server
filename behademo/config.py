from flask_env import MetaFlaskEnv


class Configuration(metaclass=MetaFlaskEnv):
    DEBUG = True
    API_ROOT = 'https://team04-18.studenti.fiit.stuba.sk/logger-server/dev'

    MONGO_URI = 'mongodb://localhost/behametrics'
    CELERY_BROKER_URL = MONGO_URI
    CELERY_RESULT_BACKEND = MONGO_URI
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_IMPORTS = [
        'behademo.services.auth.tasks'
    ]
