from celery import Celery

from behademo import Configuration


def create_celery_app():
    app = Celery(__name__)

    app.config_from_object(Configuration, namespace='CELERY')

    return app


celery = create_celery_app()
