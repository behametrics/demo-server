from json import JSONDecodeError

from flask import Blueprint, jsonify
from webargs import ValidationError
from werkzeug.exceptions import UnprocessableEntity

blueprint = Blueprint('error', __name__)


def make_error_response(message, error_code, code, detail=None):
    return jsonify({
        'status': code,
        'code': error_code,
        'message': message,
        'detail': detail
    }), code


@blueprint.app_errorhandler(JSONDecodeError)
def handle_json_decode_error(e: JSONDecodeError):
    return make_error_response(message='Unable to parse given JSON',
                               error_code='request/illegal-json',
                               detail=str(e),
                               code=400)


@blueprint.app_errorhandler(422)
def handle_validation_error(e: UnprocessableEntity):
    exc = getattr(e, 'exc')

    if isinstance(exc, ValidationError):
        return make_error_response(message='Argument didn\'t pass validation',
                                   error_code='request/validation-failure',
                                   detail=exc.messages,
                                   code=422)

    return make_error_response(message="Invalid request",
                               error_code='request/unknown-error',
                               code=422)


class ModelNotFound(Exception):

    def __init__(self, username):
        super().__init__(f'Model for user {username} not trained yet')


@blueprint.app_errorhandler(ModelNotFound)
def handle_model_not_found(e: ModelNotFound):
    return make_error_response(message='Model not found',
                               error_code='request/wrong-value',
                               detail=str(e),
                               code=400)


class InvalidSession(Exception):

    def __init__(self, session_id):
        super().__init__(f'sessionId: {session_id} is not valid')


@blueprint.app_errorhandler(InvalidSession)
def handle_invalid_session(e: InvalidSession):
    return make_error_response(message='Session not valid',
                               error_code='request/invalid-session',
                               detail=str(e),
                               code=400)
