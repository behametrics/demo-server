#!/bin/sh

mkdir -p /var/run/celery
mkdir -p /var/log/celery

celery multi start w1 -l info \
    -A behademo.celery_config.celery \
    --pidfile="/var/run/celery/%n.pid" \
    --logfile="/var/log/celery/%n%I.log"

exec gunicorn -b :5000 --workers 3 --access-logfile - --error-logfile - autoapp:app
