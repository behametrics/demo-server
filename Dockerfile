FROM fastar/ml-base:slim

COPY behademo /app/behademo
COPY requirements.txt /app
COPY docker-entrypoint.sh /app
COPY autoapp.py /app
WORKDIR /app

RUN pip install -r requirements.txt
RUN pip install gunicorn

ENV VERSION=VERSION

EXPOSE 5000
ENTRYPOINT [ "./docker-entrypoint.sh" ]
